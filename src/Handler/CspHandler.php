<?php

namespace Drupal\dd_csp_generator\Handler;

use Drupal\Core\Config\ConfigFactory;
use Drupal\dd_csp_generator\Form\ViolationsForm;
use Drupal\dd_csp_generator\Form\WhitelistForm;
use Drupal\dd_csp_generator\State\CspStateManager;


class CspHandler
{
    const STATE_PREFIX = 'dd_csp_logger.';
    const VIOLATION_PREFIX = 'violation.';

    const SEC_KIT_SETTINGS_KEY = 'seckit.settings';

    private $cspStateManager;
    private $configFactory;

    public function __construct(CspStateManager $cspStateManager, ConfigFactory $configFactory)
    {
        $this->cspStateManager = $cspStateManager;
        $this->configFactory = $configFactory;
    }

    public function saveViolation(array $context)
    {
        $directive = $context['@directive'];
        $blockedUri = $context['@blocked_uri'];
        $uid = $context['uid'];
        $requestUri = $context['request_uri'];
        $referer = $context['referer'];
        $ip = $context['ip'];
        $timestamp = $context['timestamp'];

        $this->cspStateManager->saveViolation($directive, $blockedUri, $uid, $requestUri, $referer, $ip, $timestamp);
        $this->whitelist($context);
    }

    public function getViolations()
    {
        return $this->cspStateManager->getViolations();
    }

    private function whitelist(array $context)
    {
        $directive = $context['@directive'];
        $blockedUri = $context['@blocked_uri'];
        $combined = sprintf('%s:%s', $directive, $blockedUri);

        $configIndex = 'seckit_xss.csp.'.$directive;

        // Get the whitelist, if not found, it's likely empty.
        $whitelist = $this->configFactory->get(WhitelistForm::CONFIG_KEY)->get(WhitelistForm::WHITELIST_INDEX);
        if($whitelist === null){
            $whitelist = [];
        }

        $policy = $this->configFactory->getEditable(self::SEC_KIT_SETTINGS_KEY)
            ->get($configIndex);

        // If the violation is in the whitelist, and not already whitelisted, add to whitelist
        if(in_array($blockedUri, $whitelist) && strpos($policy, $this->encodeViolation($blockedUri)) === false){
            $policy = sprintf('%s %s', $policy, $this->encodeViolation($blockedUri));
            $this->configFactory->getEditable(self::SEC_KIT_SETTINGS_KEY)->set($configIndex, $policy)->save();
        }

        // If the violation is in the whitelist, and not already whitelisted, add to whitelist
        if(in_array($combined, $whitelist) && strpos($policy, $this->encodeViolation($blockedUri)) === false){
            $policy = sprintf('%s %s', $policy, $this->encodeViolation($blockedUri));
            $this->configFactory->getEditable(self::SEC_KIT_SETTINGS_KEY)->set($configIndex, $policy)->save();
        }
    }

    private function isCustomViolation($violation){
        if($violation === 'none'){
            $isCustom = true;
        }else if($violation === 'self'){
            $isCustom = true;
        }else if($violation === 'inline'){
            $isCustom = true;
        }else if($violation === 'eval'){
            $isCustom = true;
        }else{
            $isCustom = false;
        }

        return $isCustom;
    }

    private function encodeViolation($violation){
        if($this->isCustomViolation($violation) && ($violation === 'inline' || $violation === 'eval')){
            $violation = '\'unsafe-'.$violation.'\'';
        }else if($this->isCustomViolation($violation)){
            $violation = '\''.$violation.'\'';
        }
        return $violation;
    }
}