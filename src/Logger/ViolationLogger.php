<?php

namespace Drupal\dd_csp_generator\Logger;

use Drupal\Core\Logger\RfcLoggerTrait;
use Drupal\Core\State\State;
use Drupal\dd_csp_generator\Handler\CspHandler;
use Psr\Log\LoggerInterface;

class ViolationLogger implements LoggerInterface
{
    const LOGGER_CHANNEL = 'seckit';

    use RfcLoggerTrait;

    private $cspHandler;

    public function __construct(CspHandler $cspHandler)
    {
        $this->cspHandler = $cspHandler;
    }

    /**
     * Logs with an arbitrary level.
     *
     * @param mixed $level
     * @param string $message
     * @param array $context
     * @return void
     * @throws \Exception
     */
    public function log($level, $message, array $context = array())
    {
        // If no channel is set, this message is not for us
        if (isset($context['channel']) === FALSE) {
            return;
        }

        // If channel is not LOGGER_CHANNEL, this message is not for us
        if($context['channel'] !== self::LOGGER_CHANNEL){
            return;
        }

        $this->cspHandler->saveViolation($context);
    }
}