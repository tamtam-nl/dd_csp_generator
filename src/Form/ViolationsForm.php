<?php

namespace Drupal\dd_csp_generator\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\dd_csp_generator\Handler\CspHandler;
use Drupal\dd_csp_generator\State\CspStateManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure example settings for this site.
 */
class ViolationsForm extends FormBase
{

    private $cspHandler;

    public function __construct(CspHandler $cspHandler)
    {
        $this->cspHandler = $cspHandler;
    }

    public static function create(ContainerInterface $container)
    {
        return new static(
            $container->get('dd_csp_generator.handler.csp')
        );
    }

    /**
     * {@inheritdoc}
     */
    public function getFormId()
    {
        return 'dd_csp_generator_violations_form';
    }

    /**
     * {@inheritdoc}
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $directives = $this->cspHandler->getViolations();

        $header = [
            CspStateManager::BLOCKED_URI_INDEX => $this->t('Blocked Uri'),
            CspStateManager::REFEREE_INDEX => $this->t('Referee'),
            CspStateManager::UID_INDEX => $this->t('Last known user'),
            CspStateManager::REFEREE_INDEX => $this->t('Last known page'),
            CspStateManager::IP_INDEX => $this->t('IP'),
            CspStateManager::TIMESTAMP_INDEX => $this->t('Last triggered on'),
            CspStateManager::VIOLATION_COUNT_INDEX => $this->t('Total violations')
        ];

        foreach ($directives as $directive => $violations) {
            $title = explode('.', $directive)[2];
            $form[$title.'_title'] = [
                '#type' => 'label',
                '#title' => $title,
            ];

            $form[$title] = [
                '#type' => 'tableselect',
                '#header' => $header,
                '#options' => array_values($violations),
                '#empty' => t('No violations found'),
            ];
        }

        return $form;
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {

    }
}