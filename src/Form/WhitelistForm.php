<?php

namespace Drupal\dd_csp_generator\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements a form to collect security check configuration.
 */
class WhitelistForm extends ConfigFormBase
{
    const CONFIG_KEY = 'dd_csp_generator.whitelist';

    const WHITELIST_INDEX = 'whitelist';

    /**
     * {@inheritdoc}.
     */
    public function getFormId()
    {
        return 'dd_csp_generator_whitelist_form';
    }

    /**
     * {@inheritdoc}.
     */
    public function buildForm(array $form, FormStateInterface $form_state)
    {
        $whitelist = $this->config(self::CONFIG_KEY)->get(self::WHITELIST_INDEX);
        if($whitelist !== null && count($whitelist) > 0){
            $whitelist = implode("\r\n", $whitelist);
        }else{
            $whitelist = '';
        }

        $form[self::WHITELIST_INDEX] = [
            '#type' => 'textarea',
            '#default_value' => $whitelist,
            '#attributes' => [
                'placeholder' => 'Enter your whitelisted urls here'
            ],
            '#description' => $this->t('<p>Enter urls in this whitelist and separate them with enters. All Urls will be whitelisted in the CSP policy automatically to prevent spam of violations.</p>
<p>You can also specify a directive for this url by prefixing it with the directive E.G: script-src:www.google.nl</p>')
        ];

        $form['actions']['submit'] = array(
            '#type' => 'submit',
            '#value' => $this->t('Save whitelist'),
            '#button_type' => 'primary'
        );

        return $form;
    }

    public function validateForm(array &$form, FormStateInterface $form_state)
    {
        if (empty($form_state->getValues()[self::WHITELIST_INDEX])) {
            $form_state->setErrorByName(self::WHITELIST_INDEX, 'The whitelist cannot be empty!');
        }
    }

    public function submitForm(array &$form, FormStateInterface $form_state)
    {
        // Retrieve the configuration
        $this->configFactory()->getEditable(self::CONFIG_KEY)
            ->set(self::WHITELIST_INDEX, explode("\r\n", $form_state->getValue(self::WHITELIST_INDEX)))
            ->save();

        parent::submitForm($form, $form_state);
    }

    /**
     * Gets the configuration names that will be editable.
     *
     * @return array
     *   An array of configuration object names that are editable if called in
     *   conjunction with the trait's config() method.
     */
    protected function getEditableConfigNames()
    {
        return [self::CONFIG_KEY];
    }
}
