<?php

namespace Drupal\dd_csp_generator\State;

use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\State\State;
use Drupal\Core\Url;


/**
 * Class LoggerService
 * @package Drupal\dd_logger\Service
 */
class CspStateManager
{
    const STATE_PREFIX = 'dd_csp_logger.';
    const VIOLATION_PREFIX = 'violation.';

    const STATE_KEYS_INDEX = self::STATE_PREFIX . self::VIOLATION_PREFIX . 'keys';

    const BLOCKED_URI_INDEX = 'blocked_uri';
    const UID_INDEX = 'uid';
    const REQUEST_URI_INDEX = 'request_uri';
    const REFEREE_INDEX = 'referee';
    const IP_INDEX = 'ip';
    const TIMESTAMP_INDEX = 'timestamp';
    const VIOLATION_COUNT_INDEX = 'violation_count';

    private $state;

    public function __construct(State $state)
    {
        $this->state = $state;
    }

    public function saveViolation($directive, $blockedUri, $uid, $requestUri, $referer, $ip, $timestamp)
    {
//        $this->state->set($this->getViolationKey($directive), []);
        // Get previous keys for easy dynamic storage
        $directives = $this->state->get(self::STATE_KEYS_INDEX, []);
        $oldViolations = $this->state->get($this->getViolationKey($directive), []);

        // Prepare new violation data
        $userUrl = Url::fromRoute('entity.user.edit_form', ['user' => $uid])->toString();
        $username = (\Drupal\user\Entity\User::load($uid))->getUsername();
        $newViolation = [
            $blockedUri => [
                self::BLOCKED_URI_INDEX => $blockedUri,
                self::REFEREE_INDEX => $referer,
                self::UID_INDEX => new FormattableMarkup('<a href=":link">@name</a>',
                    [
                        ':link' => $userUrl,
                        '@name' => $username
                    ]
                ),
                self::REQUEST_URI_INDEX => $requestUri,
                self::IP_INDEX => $ip,
                self::TIMESTAMP_INDEX => date('d-M-Y', $timestamp),
                self::VIOLATION_COUNT_INDEX => $this->getCount($directive, $blockedUri)
            ]
        ];

        // Merge the old violations with the new violation
        $this->state->set($this->getViolationKey($directive), array_merge($oldViolations, $newViolation));

        // Update directive
        $this->state->set(self::STATE_KEYS_INDEX, $directives);

        // Add directive to dynamic storage keys
        $directives[$directive] = true;
        $this->state->set(self::STATE_KEYS_INDEX, $directives);
    }

    public function getViolationKeys($prefixed = false, $asValues = false)
    {
        $keys = $this->state->get(self::STATE_KEYS_INDEX);

        if (!is_array($keys)) {
            return [];
        }

        if ($prefixed === true) {
            foreach ($keys as $key => $value) {
                $keys[$this->getViolationKey($key)] = $keys[$key];
                unset($keys[$key]);
            }
        }

        if ($asValues === true) {
            $keys = array_keys($keys);
        }

        return $keys;
    }

    public function getViolations(array $keys = [])
    {
        if (count($keys) === 0) {
            $keys = $this->getViolationKeys(true, true);
        }

        return $this->state->getMultiple($keys);
    }

    private function getViolationKey($directive)
    {
        return self::STATE_PREFIX . self::VIOLATION_PREFIX . $directive;
    }

    private function getCount($directive, $blockedUri)
    {
        $count = 1;

        $previous = $this->state->get($this->getViolationKey($directive));
        if (isset($previous[$blockedUri][self::VIOLATION_COUNT_INDEX])) {
            $count = $previous[$blockedUri][self::VIOLATION_COUNT_INDEX];
            $count++;
        }

        return $count;
    }
}